def min_index(arr):
    min=float('inf')
    for i in range(0,len(arr)):
        if arr[i]<min:
            min=arr[i]
            j=i
    return j

def nearestUserIP():
    users = []
    userIP = []
    userdist = []
    
    txtfile = open("distances.txt","r+")
    data=txtfile.read().split('\n')
    for x in data:
        users.append(x)
        
    for i in range(0, len(users), 2):
        userdist.append(float(users[i]))
    
    for i in range(1, len(users), 2):
            userIP.append(str(users[i]))   

    return userIP[min_index(userdist)]

def suitableNode():
    responseSort()
    nodeFound=0
    with open('nodes.txt', 'r') as fp:  
       while (nodeFound==0):  
           for line in fp:
               print(line)
               port, IP, passed, alive, resp = (
                           item.strip() for item in line.split(','))
               if (alive=="Alive") and (passed=="NotPassed"):
                   nodeFound=1
                   break ## think about eof
    return port, IP          
           
def wasThatNode(fromIP):
    Node = False
    ## We know that message was recieved by the node if its IP is in our list of nodes
    ## TODO: add ports
    with open('nodes.txt', 'r') as fp:
           for line in fp:
               IP, passed, alive, resp = (
                           item.strip() for item in line.split(','))
               if (fromIP==IP):
                   Node = True
                   break
    return Node

def fromNodeToUser(message):
    separatedMessage = []
    ## We are to recieve specific type messages from the nodes which should contain user's IP it is supposed to be sent to.
    ## We need to extract IP from the message and send the remains to this IP
    ## Ideally IP is the first thing before "-"
    IP, theRest = (
                item.strip() for item in message.split('-', 1)) ## We are splitting only on the first occurence
    separatedMessage.append(IP)
    separatedMessage.append(theRest)
    return separatedMessage

## We can sort the nodes by response times and always choose the best ones.

def responseSort():
    IPList = []
    passedList = []
    aliveList = []
    respList = []
    portlist = []
    with open('nodes.txt', 'r') as fp:
        for line in fp:
            ##print(line)
            port, IP, passed, alive, resp = (
                        item.strip() for item in line.split(','))
            portlist.append(port)
            IPList.append(IP)
            passedList.append(passed)
            aliveList.append(alive)
            respList.append(float(resp))
    fp.close()
    
    for i in range(len(IPList)):
        for j in range(0, len(IPList)-i-1):
            if respList[j] > respList[j+1]:
                respList[j], respList[j+1] = respList[j+1], respList[j]
                IPList[j], IPList[j+1] = IPList[j+1], IPList[j] 
                aliveList[j], aliveList[j+1] = aliveList[j+1], aliveList[j] 
                passedList[j], passedList[j+1] = passedList[j+1], passedList[j] 
                portlist[i], portlist[j+1] = portlist[j+1], portlist[i]
               
    fp=open("nodes.txt","w+")
    for i in range(len(IPList)):
        fp.write("{0},{1},{2},{3},{4}\n".format(portlist[i],IPList[i],passedList[i],aliveList[i],respList[i]))
    fp.close()


def setDead(fromIP):
    with open('nodes.txt', 'rt') as fp:        
        for line in fp:
            port, IP, passed, alive, resp = (
                        item.strip() for item in line.split(','))
            if (fromIP==IP):
                newLine=line.replace('Dead', 'Alive')
               ##f = open("nodes.txt", 'w')
               ## f.write(fp.read())
               ## f.close()  
               
    with open('nodes.txt', "w") as f:
        f.write(fp)
 
