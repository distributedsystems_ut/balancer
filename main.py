from auxfunc import min_index, nearestUserIP, suitableNode, wasThatNode, fromNodeToUser, responseSort, setDead
from threading import Thread
import threading
import time
import logging
logging.basicConfig(level=logging.DEBUG,\
                    format='%(asctime)s (%(threadName)-2s) %(message)s',)
from socket import AF_INET, SOCK_STREAM, socket, SHUT_WR
from socket import error as soc_err
import re
##import ndb
users = dict()
requests =dict()
class ClientHandler(Thread):

    
    def __init__(self,client_socket,client_addr, balancer, buff_size=1024, encode = 'utf-8'):
        Thread.__init__(self)
        self.__client_socket = client_socket
        self.__client_address = client_addr
        self.__buff_size = buff_size
        self.__encode = encode
        self.__balancer = balancer
        print("INITIALIZED", balancer, self.__balancer)
        ##self.__nodes_file = nodes_file
        
    def run(self):
        self.__handle()

    def handle(self):
        self.start()
        
    def __initial_parse(self,m):
        n,x= re.split('[|]', m)
        return str(n),str(x)
    
        
    def __parse_message_from_node(self, m):
        n, x, z, y = re.split('[$$|]', m)
        return int(n), str(x), str(z), str(y)   


    def __parse_message(self, m):
        print("MMMMM", m)
        n = 0
        if '/' in m:
            n, x, y = re.split('[/|]', m)
        else:
            x, y = re.split('[|]', m)
        return int(n), str(x), str(y)
    


    def __parse_line(self, line):
        port, addr, passed, alive, response = re.split(',', line)
        return int(port), str(addr), str(passed), str(alive), float(response)
    
    def __identify_sender(self, client_address):
        f = open(file_name, 'r')
        line = f.readline()
        senderNode=False
        port, addr, passed, alive, response = self.__parse_line(line)
        while True:
            line = f.readline()
            if not line:
                break
            port, addr, passed, alive, response = self.__parse_line(line)
            senderNode=senderNode or (addr==client_address)
        f.close()        
        return senderNode

    
    def addUser(self,x,client_address):
        users[x]=client_address
    


    def __handle(self):
        while True:
            success, _ = self.process()
            print(success)
            if not success: return
    
    def process(self):

        message = self.__client_socket.recv(self.__buff_size)
        return self.processMessage(message)        
        try:
            raise KeyboardInterrupt
            
        except soc_err as e:
            print("fucking", e)
            if e.errno == 107:
                logging.warn( 'Client %s:%d left before server could handle it'\
                              '' %  self.__client_address )
            else:
                logging.error( 'Error: %s' % str(e) )
        except:
            print("fucking fuck")
        finally:
            pass
            ##self.__client_socket.close()
            ##logging.info( 'Client %s:%d disconnected' % self.__client_address )        
        
        return True
    
    def processMessage(self, message):
        umessage = message.decode(self.__encode)
        logging.info('Received message: %s' % umessage)
        if umessage=="":
            ##port, IP = suitableNode()
            ##s.connect((IP, port))
            ##s.sendall(umessage.encode(self.__encode))
            print('wtf?1',message)
            self.__client_socket.shutdown(SHUT_WR)
            print('wtf?2',self.__balancer)
            print(self)
            self.__balancer.handlers.remove(self)
            self.__balancer = None
            print('wtf?3',message)
            return False, None
            
        else:
            n, x, y=self.__parse_message(umessage)
            print(n,x,y)

            if y[0]=="p":
           
                ####request: 1/32|p
                ####response: 32|p                    
                self.__client_socket.sendall((str(x)+'|'+str(y)).encode(self.__encode))
                ##self.__client_socket.shutdown(SHUT_WR)
                logging.info('Sent message: %s' % str(x)+'|'+str(y))
        
            elif y[0]=="i":
                    ####request: 1/273|i:isdfsh34543jef
                    ####response: 273|i:123.231.13.1
                response_message = x + "|i:"
                user_id=y.split(":")[1]
                if user_id in self.__balancer.users:
                    requested_ip = self.__balancer.users[user_id]
                    response_message += requested_ip[0]
                else:
                    response_message += "444.444.444.444"
                    
                self.__client_socket.sendall(response_message.encode(self.__encode))
                ##self.__client_socket.shutdown(SHUT_WR)
                logging.info('Sent message: %s' % response_message) 
        
            elif y[0]=="n":
                if n==0:
                    self.__balancer.users[umessage.split(':')[-1]] = self.__client_address
                    self.__client_socket.sendall(message)
                else:  
                    return self.sendAndListenToMahdi(umessage)                    
                    
                ####request: 1/45|n:10011
                ####response: 45|isoaf31sdasw21 this response we await from node.
        
            elif y[0]=="m":
                self.__client_socket.sendall(umessage.encode(self.__encode))
        
            else:
                return self.sendAndListenToMahdi(umessage)
                
            return True, x
            
    def sendAndListenToMahdi(self, umessage):
        _, x1, _=self.__parse_message(umessage)
        port, IP = suitableNode()
        s = self.__balancer.nodes[IP+port]
        s.sendall(umessage.encode(self.__encode))
        proper_response = False
        while not proper_response:
            message = s.recv(self.__buff_size)
            umessage = message.decode(self.__encode)
            logging.info('Received message: %s' % umessage)
            if umessage=="": proper_response = True
            else:
                _, x2, _=self.__parse_message(umessage)
                proper_response = x1 == x2
                
        return self.processMessage(message)
    
    def __nearest_node(self,file_name):
        responseSort()
        f = open(file_name, 'r')
        line = f.readline()
        port, addr, passed, alive, response = self.__parse_line(line)

        while True:
            line = f.readline()
            if not line:
                break
            port, addr, passed, alive, response = self.__parse_line(line)
        f.close()
        return addr.encode("utf-8")
    
    def __update_message(self, m, client_address, port):
        n, x, y = re.split('/|,', m)
        return n+str('/')+x+str('$')+str(client_address)+str('$')+str(port)+str('|')+y
    
   
class Balancer():
    nodes = {}
    users = dict()
    
    def __init__(self,nodes_file):
        self.handlers = []
        self.__nodes_file = nodes_file
        self.connect()
        
    def connect(self):
        s = socket(AF_INET, SOCK_STREAM)
        #s.settimeout(4.0)
        fp = open('nodes.txt', 'r')
        for line in fp:
            port, IP, passed, alive, resp = (
                item.strip() for item in line.split(','))
            print(port, IP, passed, alive, resp)
            try:
                s.connect((IP, int(port)))
                self.nodes[IP+port]=s 
            except:                  
                print("fuck")
                pass
            ##setDead(IP)                    
        print(self.nodes)

        ##socket.gettimeout()
        ##socket.settimeout(value)
        
    def listen(self,sock_addr,backlog=1):
        self.__sock_addr = sock_addr
        self.__backlog = backlog
        self.__s = socket(AF_INET, SOCK_STREAM)
        self.__s.bind(self.__sock_addr)
        self.__s.listen(self.__backlog)
        logging.debug( 'Socket %s:%d is in listening state'\
                       '' % self.__s.getsockname() )

    def loop(self):
        
        logging.info( 'Falling to serving loop, press Ctrl+C to terminate ...' )

        client_socket = None        
        try:
            while 1:
                logging.info( 'Awaiting new clients ...' )
                client_socket,client_addr = self.__s.accept()
                logging.info( 'New client connected from %s:%d'\
                                      '' % client_addr )
                logging.info('Service offered, client %s:%d is using a service ...' \
                                     '' % client_addr)
                ##nodes_file='nodes.txt'
                c = ClientHandler(client_socket,client_addr, self)
                self.handlers.append(c)
                c.handle()
        except:
            logging.warn( 'ISSUE ...' )
            if client_socket != None:
                client_socket.close()
            self.__s.close()     

if __name__ == '__main__':
    
    logging.info( 'Application started' )
    b = Balancer('nodes.txt')
    b.listen(('',7779))
    b.loop()
    
    logging.info ( 'Terminating ...' )  